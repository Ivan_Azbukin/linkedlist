﻿namespace LinkedList.Dto
{
    public struct SerializedListNodeDto
    {
        
        public int Current { get; set; }
        
        /// <summary>
        /// Id случайного элемента, может быть null
        /// </summary>
        public int? Random { get; set; }

        /// <summary>
        /// Payload
        /// </summary>
        public string Data { get; set; }
    }
}