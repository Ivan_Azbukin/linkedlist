// ## Test
//
// Consider the following data structure and interface:
// ``` 

// Please provide the following:
//
// 1. Develop class implementing IListSerializer interface for time critical part of the back-end application.
// You can use any serialization format but you could not utilize 3d party libraries that will serialize the full list for you.
// I.e. it's allowed to utilize 3d party libraries for serializing 1 node in particular format.
// Note:
// - provided data structures could not be changed;
// - solution is allowed to be not thread safe;
// - it's guaranteed that list provided as an argument to Serialize and DeepCopy function is consistent and doesn't contain any cycles;
// - automated testing of your solution will be performed, the resulting rate for the solution will be given based on (in order of priority):
//   - tests on correctness of the solution 
//   - performance tests 
//   - tests on memory consumption
//
// 2. Provide your own test cases for the implementation.