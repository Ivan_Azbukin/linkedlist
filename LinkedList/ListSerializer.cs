﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using LinkedList.Dto;

namespace LinkedList
{
    public class ListSerializer: IListSerializer
    {
        public async Task Serialize(ListNode head, Stream s)
        {
            if (head == null)
            {
                return;
            }
            var listItem = head;
            await using var streamWriter = new StreamWriter(s, leaveOpen: true);
            while (true)
            {
                await streamWriter.WriteLineAsync(JsonSerializer.Serialize(new SerializedListNodeDto()
                {
                    Data = listItem.Data,
                    Random = listItem.Random?.GetHashCode(),
                    Current = listItem.GetHashCode()
                }));
                
                if (listItem.Next == null)
                {
                    return;
                }

                listItem = listItem.Next;
            }
            
        }
        

        /// <summary>
        /// Десериализует один элемент из строки
        /// </summary>
        /// <param name="line">сериализованная строка</param>
        /// <param name="itemsDict">словарь всех элементов</param>
        /// <param name="prevItem">ссылка на предыдущий элемент</param>
        /// <returns>Десериализованный элемент</returns>
        /// <exception cref="ArgumentException">Thrown when a stream has invalid data</exception>
        private ListNode DeserializeItem(string line, Dictionary<int, ListNode> itemsDict, ListNode prevItem)
        {
            SerializedListNodeDto dtoItem;
            try
            {
                dtoItem = JsonSerializer.Deserialize<SerializedListNodeDto>(line);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Stream has invalid data", ex);
            }
            
            ListNode currentItem;
            //Если очередной элемент уже есть в словаре  элементов
            //то берём его
            if (itemsDict.ContainsKey(dtoItem.Current))
            {
                currentItem = itemsDict[dtoItem.Current];
                currentItem.Data = dtoItem.Data;
                currentItem.Previous = prevItem;
            }
            //Иначе создаём новый
            else
            {
                currentItem = new ListNode()
                {
                    Data = dtoItem.Data,
                    Previous = prevItem
                };
                //И добавляем его в словарь 
                itemsDict[dtoItem.Current] = currentItem;
            }

            //Если у элемента есть ссылка на случайный из коллекции,
            //то проверяем, есть ли уже этот элемент в нашем словаре
            if (dtoItem.Random.HasValue)
            {
                //Если есть, то просто присваиваем
                if (itemsDict.ContainsKey(dtoItem.Random.Value))
                {
                    currentItem.Random = itemsDict[dtoItem.Random.Value];
                }
                //Иначе инициализируем новый и добавляем его словарь
                else
                {
                    currentItem.Random = new ListNode();
                    itemsDict[dtoItem.Random.Value] = currentItem.Random;
                }
            }

            return currentItem;
        }
        
        public async Task<ListNode> Deserialize(Stream s)
        {
            using var streamReader = new StreamReader(s, leaveOpen:true);

            var itemsDict = new Dictionary<int, ListNode>();
            
            var headLine = await streamReader.ReadLineAsync();

            var head = DeserializeItem(headLine, itemsDict, null);
            
            var prevItem = head;
            string line;
            while ((line = await streamReader.ReadLineAsync()) != null)
            {
                var currentItem = DeserializeItem(line, itemsDict, prevItem);

                prevItem.Next = currentItem;
                prevItem = currentItem;
            }

            return head;
        }
        
        /// <summary>
        /// Создаёт копию одного элемента
        /// </summary>
        /// <param name="itemsDict">Словарь всех элементов</param>
        /// <param name="srcItem">Ссылка на исходный элемент</param>
        /// <param name="dstPrevItem">Сслылка на предыдущий элемент копии</param>
        /// <returns>Копию элемента srcItem</returns>
        private ListNode DeepCopyItem(Dictionary<ListNode, ListNode> itemsDict, ListNode srcItem, ListNode dstPrevItem)
        {
            ListNode dstCurrentItem;

            //Если очередной элемент уже есть в словаре  элементов
            //то берём его
            if (itemsDict.ContainsKey(srcItem))
            {
                dstCurrentItem = itemsDict[srcItem];
                dstCurrentItem.Data = srcItem.Data;
                dstCurrentItem.Previous = dstPrevItem;
            }
            //Иначе создаём новый
            else
            {
                dstCurrentItem = new ListNode()
                {
                    Data = srcItem.Data,
                    Previous = dstPrevItem
                };
                //И добавляем его в словарь 
                itemsDict[srcItem] = dstCurrentItem;
            }

            //Если у элемента есть ссылка на случайный из коллекции,
            //то проверяем, есть ли уже этот элемент в нашем словаре
            if (srcItem.Random != null)
            {
                //Если есть, то просто присваиваем
                if (itemsDict.ContainsKey(srcItem.Random))
                {
                    dstCurrentItem.Random = itemsDict[srcItem.Random];
                }
                //Иначе инициализируем новый и добавляем его словарь
                else
                {
                    dstCurrentItem.Random = new ListNode();
                    itemsDict[srcItem.Random] = dstCurrentItem.Random;
                }
            }

            return dstCurrentItem;
        }
        
        public async Task<ListNode> DeepCopy(ListNode head)
        {
            return await Task.Run(() =>
            {
                //В словаре ключ - исходный элемент, значение - копия
                var itemsDict = new Dictionary<ListNode, ListNode>();

                var dstHead = DeepCopyItem(itemsDict, head, null);
                
                var srcItem = head;
                var dstPrevItem = dstHead;
                while ((srcItem = srcItem.Next) != null)
                {
                    var dstCurrentItem = DeepCopyItem(itemsDict, srcItem, dstPrevItem);

                    dstPrevItem.Next = dstCurrentItem;
                    dstPrevItem = dstCurrentItem;
                    
                }
                return dstHead;
            });
            
        }

       
    }
}