﻿using System;
using System.Collections.Generic;
using LinkedList;

namespace UnitTests
{
    public static class LinkedListBuilder
    {
        /// <summary>
        /// Создаёт список без рандомных элементов
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static ListNode CreateBase(int length)
        {
            var head = new ListNode()
            {
                Data = "0"
            };

            var prevItem = head;
            for (int i = 1; i < length; ++i)
            {
                var currentItem = new ListNode()
                {
                    Data = i.ToString(),
                    Previous = prevItem
                };
                prevItem.Next = currentItem;
                prevItem = currentItem;
            }
            return head;
        }

        /// <summary>
        /// Добавляет ссылки на рандомные элементы
        /// </summary>
        /// <param name="head">Первый элемент листа</param>
        /// <param name="prob">Вероятность добавления случайного элемента</param>
        /// <returns></returns>
        public static ListNode WithRandoms(this ListNode head, double prob)
        {
            var listItem = head;
            List<ListNode> nodesList = new List<ListNode>();
            while (true)
            {
                nodesList.Add(listItem);
                if (listItem.Next == null)
                {
                    break;
                }
                listItem = listItem.Next;
            }

            var rnd = new Random();
            
            for (int i = 0; i < nodesList.Count; ++i)
            {
                if (rnd.NextDouble() < prob)
                {
                    nodesList[i].Random = nodesList[rnd.Next(0, nodesList.Count)];
                }
            }
            
            return head;
        }

        public static ListNode SetRandomAt(this ListNode head, ListNode random, int position)
        {
            var listItem = head;
            int counter = 0;
            while (true)
            {
                if (counter != position)
                {
                    counter++;
                    listItem = listItem.Next;
                    continue;
                }

                listItem.Random = random;
                return head;
            }
        }
    }
}