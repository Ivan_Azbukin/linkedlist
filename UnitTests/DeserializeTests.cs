﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using LinkedList;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests
{
    public class DeserializeTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public DeserializeTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public async Task Deserialize_CorrectInput_ListDeserialized()
        {
            //arrange
            var serializedString = @"{""Current"":48611003,""Random"":48611003,""Data"":""0""}
            {""Current"":55467396,""Random"":null,""Data"":""1""}
            {""Current"":31423778,""Random"":48611003,""Data"":""2""}";
            
            var linkedList = LinkedListBuilder
                .CreateBase(3);
            linkedList.SetRandomAt(linkedList, 2);
            
            var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(serializedString));
            //act
            var listSerializer = new ListSerializer();
            var head = await listSerializer.Deserialize(memoryStream);
            
            //assert
            var listItem = head;
            for (int i = 0; i < 3; ++i)
            {
                listItem.Data.Should().Be(i.ToString());
                //тут проверяем, что у последнего элемента случайным оказался первый
                if (i == 2)
                {
                    listItem.Random.Data.Should().Be(0.ToString());
                }
                listItem = listItem.Next;
            }

            //Проверяем, что мы действительно прошли весь список
            listItem.Should().BeNull();

        }

        [Fact]
        public async Task Deserialize_IncorrectInput_ThrowsArgumentException()
        {
            //arrange
            var inputString = "bad input";
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(inputString));
            
            //act
            var serializer = new ListSerializer();
            Func<Task<ListNode>> act = async () => await serializer.Deserialize(stream);
            
            //assert
            await act.Should().ThrowAsync<ArgumentException>();
        }

        [Theory]
        [InlineData(10)]
        [InlineData(100)]
        [InlineData(1000)]
        [InlineData(10000)]
        [InlineData(100000)]
        public async Task Deserialize_Performance_Tests(int length)
        {
            var runsList = new List<double>();

            for (int run = 0; run < 100; ++run)
            {
                //arrange
                var list = LinkedListBuilder.CreateBase(length).WithRandoms(0.5);
                var serializer = new ListSerializer();
                var stream = new MemoryStream();

                await serializer.Serialize(list, stream);
                stream.Seek(0, SeekOrigin.Begin);
            
                //act
                var sw = new Stopwatch();
                sw.Start();
                await serializer.Deserialize(stream);
                sw.Stop();
                runsList.Add(sw.Elapsed.TotalMilliseconds);
            }
           
            _testOutputHelper.WriteLine($"Elements: {length}, time elapsed (ms): {runsList.Sum()/runsList.Count}");
        }
    }
}