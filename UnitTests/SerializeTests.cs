using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using LinkedList;
using LinkedList.Dto;
using Xunit;
using Xunit.Abstractions;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace UnitTests
{
    public class SerializeTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public SerializeTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public async Task Serialize_CorrectInput_ListSerialized()
        {
            //arrange
            var length = 1;
            var head = LinkedListBuilder
                .CreateBase(length)
                .WithRandoms(0.5);
            
            await using var stream = new MemoryStream();

            var listSerializer = new ListSerializer();
            
            //act
            await listSerializer.Serialize(head, stream);
            
            //assert
            stream.Seek(0, SeekOrigin.Begin);
            using var sr = new StreamReader(stream, leaveOpen: true);
            var dtoDict = new Dictionary<int, SerializedListNodeDto>();
            string line;
            while ((line = await sr.ReadLineAsync()) != null)
            {
                var item = JsonSerializer.Deserialize<SerializedListNodeDto>(line);
                dtoDict[item.Current] = item;
            }
            
            dtoDict.Count.Should().Be(length);
            
            var linkedListItem = head;
            foreach (var dtoItem in dtoDict.Values)
            {
                //Проверяем, что совпадают данные
                dtoItem.Data.Should().Be(linkedListItem.Data);
                //Проверяем, что совпадают случайные елементы
                if (dtoItem.Random != null)
                {
                    dtoDict[dtoItem.Random.Value].Data.Should().Be(linkedListItem.Random.Data);
                }
                linkedListItem = linkedListItem.Next;
            }
        }

        [Theory]
        [InlineData(10)]
        [InlineData(100)]
        [InlineData(1000)]
        [InlineData(10000)]
        [InlineData(100000)]
        public async Task Serialize_Performance_Test(int length)
        {
            //var length = 100;
            var runsList = new List<double>();
            for (int run = 0; run < 100; ++run)
            {
                //arrange
                var head = LinkedListBuilder
                    .CreateBase(length)
                    .WithRandoms(0.5);

                await using var stream = new MemoryStream();
                var listSerializer = new ListSerializer();

                var sw = new Stopwatch();
                sw.Start();
                //act
                await listSerializer.Serialize(head, stream);
                sw.Stop();

                runsList.Add(sw.Elapsed.TotalMilliseconds);
            }

            _testOutputHelper.WriteLine($"Elements: {length}, time elapsed (ms): {runsList.Sum() / runsList.Count}");
        }
    }
}