﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using LinkedList;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests
{
    public class DeepCopyTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public DeepCopyTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public async Task DeepCopy_CorrectInput_CopyCreated()
        {
            //arrange
            var srcHead= LinkedListBuilder
                .CreateBase(10)
                .WithRandoms(0.5);
            //act
            var serializer = new ListSerializer();
            var dstHead = await serializer.DeepCopy(srcHead);

            var srcListItem = srcHead;
            var dstListItem = dstHead;
            //assert
            while (true)
            {
                srcListItem.Data.Should().Be(dstListItem.Data);
                if (srcListItem.Random != null)
                {
                    srcListItem.Random.Data.Should().Be(dstListItem.Random.Data);
                }
                else
                {
                    dstListItem.Random.Should().BeNull();
                }

                if (srcListItem.Next == null)
                {
                    return;
                }
                srcListItem = srcListItem.Next;
                dstListItem = dstListItem.Next;
            }
        }

        [Theory]
        [InlineData(10)]
        [InlineData(100)]
        [InlineData(1000)]
        [InlineData(10000)]
        [InlineData(100000)]
        public async Task DeepCopy_Performance_Tests(int length)
        {
            var runsList = new List<double>();
            for (int run = 0; run < 100; ++run)
            {
                //arrange
                var srcHead= LinkedListBuilder
                    .CreateBase(length)
                    .WithRandoms(0.5);
                
                //act
                var serializer = new ListSerializer();
                var sw = new Stopwatch();
                sw.Start();
                await serializer.DeepCopy(srcHead);
                sw.Stop();
                runsList.Add(sw.Elapsed.TotalMilliseconds);
            }
            
            _testOutputHelper.WriteLine($"Elements: {length}, time elapsed (ms): {runsList.Sum()/runsList.Count}");
        }
        
    }
}